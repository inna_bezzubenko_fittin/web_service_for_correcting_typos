Алгоритм:
1. Считать ввод
2. Создать множество опечаток
3. Создать словарь замен, исходя из анализа соответствия
4. Заменить все вхождения слов с опечатками

Использованные ссылки:

- https://vc.ru/dev/174452-kak-proverit-pravopisanie-s-pomoshch-python-pyenchant
- https://pyenchant.github.io/pyenchant/api/enchant.pypwl.html
- https://we-are.bookmyshow.com/building-a-movies-dictionary-spell-checker-7dd6f6d897ff