import enchant
import difflib
from enchant.checker import SpellChecker
from enchant.tokenize import EmailFilter, URLFilter
import logging

# logging.basicConfig(level=logging.DEBUG)

language = "ru_RU"  # "en_US"

# load the personal word list dictionary
shoes_dict = enchant.PyPWL("shoes.txt")


def get_list_typos(txt: str) -> list:
    checker_with_filters = SpellChecker(language, filters=[EmailFilter])
    checker_with_filters.set_text(txt)
    typos_list = [i.word for i in checker_with_filters]
    return typos_list


# Create a dictionary of substitutions
def make_dict_changes(typos_list: list) -> dict:
    dictionary_of_possible_changes = {}
    print(typos_list)
    for wrong_word in typos_list:
        # the code uses its own dictionary of shoe names
        suggestions = shoes_dict.suggest(wrong_word)
        dictionary_of_possible_changes[wrong_word] = suggestions[:3]

        measure = difflib.SequenceMatcher(None, wrong_word, dictionary_of_possible_changes[wrong_word][0]).ratio()
        if measure < 0.8:
            # the code uses the built-in dictionary of the Russian language
            dictionary = enchant.Dict(language)
            suggestions = list(set(dictionary.suggest(wrong_word)))
            dictionary_of_possible_changes[wrong_word] = suggestions

    return dictionary_of_possible_changes


def choosing_replacement(dict_changes: dict) -> dict:
    result_dict = {}
    for typo in dict_changes.keys():
        print(f'выберете номер замены для слова "{typo}"')
        for i, word in enumerate(dict_changes[typo]):
            print(f"{i}: {word}")
        number = int(input())
        result_dict[typo] = dict_changes[typo][number]
    return result_dict


def apply_changes(txt: str, result_dict: dict) -> str:
    new_text = txt
    for old, new in result_dict.items():
        new_text = new_text.replace(old, new)
    return new_text


if __name__ == '__main__':
    text = input()
    typos_list = get_list_typos(text)
    dict_changes = make_dict_changes(typos_list)
    logging.debug(dict_changes)
    result_dict = choosing_replacement(dict_changes)
    print(apply_changes(text, result_dict))
